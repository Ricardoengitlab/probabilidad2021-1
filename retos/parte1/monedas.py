import random

caras, cruces = 0, 0
for i in range(0, 100):
    valor = random.randint(0, 1)
    if valor:
        caras += 1
    else:
        cruces += 1

print(f"Hubo un total de {caras} caras")
print(f"Hubo un total de {cruces} cruces")
