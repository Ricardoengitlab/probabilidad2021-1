import time
import random

castigos = ["hacer 10 sentadillas.", "hacer 5 lagartijas.",
            "hacer 15 abdominales.", "quedarte sin postre."]

number = random.randint(1, 100)
intentos = 1

print("Tienes un minuto después de la primer respuesta y 8 intentos")
print("El numero está entre 1 y 100 ")

res = int(input("> "))
inicio = int(time.time())

while intentos < 8:
    if res < number:
        print("Elige un numero mayor")
    else:
        print("Elige un numero menor")

    res = int(input("> "))
    intentos += 1

    if res == number:
        print(f"¡Correcto! El número es {number}")
        break

    if int(time.time()) - inicio > 60:
        print("Te tardaste mucho")
        print(f"El número era {number}")
        break

if intentos > 8:
    print("Muchos intentos")
    print(f"El número era {number}")
